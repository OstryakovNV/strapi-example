module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  app: {
    keys: ['wO+8s7zicOtnDY5CMQ4yng==', '984UR8QCst2aW9KOHkAk/Q==', 'XYCGTYQ17Stt0E0wgufXHw==', 'TZMus5tcKP349LJic0viBg=='],
  },
});
